package com.jd.objects;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnumTest {
    private static final Logger logger = LoggerFactory.getLogger(EnumTest.class);

    public enum SetFilter {
        EQUAL("="), GREATER_THAN(">"), GREATER_THAN_OR_EQAL(">="), LESS_THAN("<=");
        private String filter;

        SetFilter(String filter) {
            this.filter = filter;
        }

        @Override
        public String toString() {
            return filter;
        }

        public static SetFilter getEnum(String filter) throws Exception {
            switch(filter) {
            case "=":
                    return EQUAL;
            case ">":
                    return GREATER_THAN;
            case ">=":
                    return GREATER_THAN_OR_EQAL;
            case "<=":
                    return LESS_THAN;

            default:
                    throw new Exception("no supported type");
            }
        }
    }

    @Test
    public void testEnum() throws Exception {
        SetFilter sf = SetFilter.getEnum(">=");
        logger.info("***** {}", sf);

    }
}
