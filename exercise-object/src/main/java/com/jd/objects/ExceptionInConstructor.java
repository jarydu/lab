package com.jd.objects;

public class ExceptionInConstructor {

    public static ExceptionInConstructor instance = new ExceptionInConstructor();

    private ExceptionInConstructor() {

        throw new RuntimeException("Haha RunTimeException");

    }

    public void doSomething() {
        System.out.println("YaHoo!!!");
    }
}
