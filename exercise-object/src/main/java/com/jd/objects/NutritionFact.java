package com.jd.objects;

public class NutritionFact {
    // required fields
    private final int servingSize;
    private final int servings;

    // optional fields
    private final int calories;
    private final int fat;
    private final int sodium;
    private final int carbohydrate;

    public static class Builder {
        // required fields
        private final int servingSize;
        private final int servings;

        // optional fields
        private int calories;
        private int fat;
        private int sodium;
        private int carbohydate;

        public Builder(int servingSize, int servings) {
            this.servingSize = servingSize;
            this.servings = servings;
        }

        public Builder calories(int calories) {
            this.calories = calories;
            return this;
        }

        public Builder fat(int fat) {
            this.fat = fat;
            return this;
        }

        public Builder sodium(int sodium) {
            this.sodium = sodium;
            return this;
        }

        public Builder carbo(int carbohydate) {
            this.carbohydate = carbohydate;
            return this;
        }

        public NutritionFact build() {
            return new NutritionFact(this);
        }
    }

    NutritionFact(Builder builder) {
        this.servingSize = builder.servingSize;
        this.servings = builder.servings;
        this.calories = builder.calories;
        this.fat = builder.fat;
        this.sodium = builder.sodium;
        this.carbohydrate = builder.carbohydate;
    }
}
