package com.jd.objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StaticFactoryMethod {
    private static final Logger LOGGER = LoggerFactory.getLogger(StaticFactoryMethod.class);

    public static boolean valueOf(boolean b) {
        return b ? true : false;
    }

    public static Object getInstance() {
        return new Object();
    }

    public static Object newInstance() {
        return new Object();
    }

    public static Object getType() {
        return new Object();
    }

    public static Object newType() {
        return new Object();
    }
}
