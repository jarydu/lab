package com.jd.objects;

public class WallE {
    // required fields
    private final int height;
    private final int length;
    private final int width;
    private final String name;

    // optional fields
    private final int speed;
    private final String parentName;
    private final int vision;

    public static final class Builder {
        private final int height;
        private final int length;
        private final int width;
        private final String name;
        private int speed = 0;
        private String parentName = null;
        private int vision = 0;

        public Builder(int height, int length, int width, String name) {
            this.height = height;
            this.length = length;
            this.width = width;
            this.name = name;
        }

        public Builder speed(int speed) {
            this.speed = speed;
            return this;
        }

        public Builder parentName(String parentName) {
            this.parentName = parentName;
            return this;
        }

        public Builder vision(int vision) {
            this.vision = vision;
            return this;
        }

        public WallE build() {
            return new WallE(this);
        }
    }

    private WallE(Builder builder) {
        this.height = builder.height;
        this.length = builder.length;
        this.width = builder.width;
        this.name = builder.name;
        this.speed = builder.speed;
        this.parentName = builder.parentName;
        this.vision = builder.vision;
    }

    @Override
    public String toString() {
        return this.height + "|" + this.length + "|" + this.width + "|" + this.name + "|" + this.speed + "|" + this.parentName + "|" + this.vision;
    }
}
