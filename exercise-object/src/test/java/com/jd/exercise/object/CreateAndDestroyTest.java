package com.jd.exercise.object;

import com.jd.objects.ExceptionInConstructor;
import com.jd.objects.NutritionFact;
import com.jd.objects.StaticFactoryMethod;
import com.jd.objects.WallE;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateAndDestroyTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateAndDestroyTest.class);

    @Test
    public void test() {
        LOGGER.info("TEST SKELETON IS READY");
    }

    @Test
    public void testStaticFactoryMethod() {
        boolean result = StaticFactoryMethod.valueOf(false);
        LOGGER.info("[{}]", result);
    }

    @Test
    public void testBuilder() {
        NutritionFact nf = new NutritionFact.Builder(1, 2).calories(100).carbo(10).fat(10).sodium(102).build();
        LOGGER.info("{}", nf);
    }

    @Test
    public void testWallE() {
        WallE wallE = new WallE.Builder(1, 3, 4, "Wall-E").parentName("Wall-A").speed(100).vision(1000).build();
        LOGGER.info("[{}]", wallE.toString());
    }

    @Test
    public void testExceptionInConstructor() {

        try {
            ExceptionInConstructor.instance.doSomething();
        } catch (Exception e) {
            LOGGER.error("Dude, that is not right");
        }
    }
}
