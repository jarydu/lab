package com.jd.exercise.reflections;

import com.jd.objects.WallE;
import org.junit.Test;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassLoadingTest {
    private static final Logger logger = LoggerFactory.getLogger(ClassLoadingTest.class);

    @Test
    public void testLoadClasses() throws Exception {
        Reflections reflections = new Reflections(new ConfigurationBuilder().setScanners(new SubTypesScanner(false), new ResourcesScanner()).setUrls(
                ClasspathHelper.forPackage("com.jd.objects")));

        for (Class clz : reflections.getSubTypesOf(WallE.class))
            logger.info("**** {}", clz.getCanonicalName());
    }
}
