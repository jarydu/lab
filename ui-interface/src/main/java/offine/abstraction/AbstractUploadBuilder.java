package offine.abstraction;

import java.io.InputStream;
import java.util.Date;

public abstract class AbstractUploadBuilder {
    protected String name;
    protected String comment;
    protected String topologyPath;
    protected String archiveName;
    protected InputStream archiveStream;
    protected String source;
    protected Date start;
    protected Date end;
    protected String cronExpression;
    protected OfflineClient.ExecutionType type;

    public AbstractUploadBuilder name(String name, String comment) {
        this.name = name;
        this.comment = comment;
        return this;
    }

    public AbstractUploadBuilder path(String topologyPath) {
        this.topologyPath = topologyPath;
        return this;
    }

    public AbstractUploadBuilder resource(String archiveName, InputStream archiveStream) {
        this.archiveName = archiveName;
        this.archiveStream = archiveStream;
        return this;
    }

    public AbstractUploadBuilder from(String source, Date start, Date end) {
        this.source = source;
        this.start = start;
        this.end = end;
        return this;
    }

    public AbstractUploadBuilder cron(String expression) {
        this.cronExpression = expression;
        return this;
    }

    public AbstractUploadBuilder now() {
        return cron(null);
    }

    public AbstractUploadBuilder type(OfflineClient.ExecutionType type) {
        this.type = type;
        return this;
    }

    public abstract void execute();
}
