package offine.abstraction;

import offine.pojo.TopologyJobInfo;

public interface OfflineClient {
    enum ExecutionType {
        LOCAL,
        DISTRIBUTED;
    }

    /**
     * list topology job info list
     */
    Iterable<TopologyJobInfo> list();

    /**
     * upload topology job into offline
     */
    AbstractUploadBuilder upload();
}
