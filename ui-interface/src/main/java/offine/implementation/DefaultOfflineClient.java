package offine.implementation;

import offine.abstraction.AbstractUploadBuilder;
import offine.abstraction.OfflineClient;
import offine.pojo.TopologyJobInfo;

public class DefaultOfflineClient implements OfflineClient {
    @Override
    public Iterable<TopologyJobInfo> list() {
        return null;
    }

    @Override
    public AbstractUploadBuilder upload() {
        return new UploadBuilder();
    }

    private static final class UploadBuilder extends AbstractUploadBuilder {

        @Override
        public void upload() {
            System.out.println("Prep is ready, let's do it");
        }
    }
}
