package offine.pojo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import offine.abstraction.OfflineClient;

public class TopologyJobInfo {
    private final String name;
    private final String comment;
    private final String topologyPath;
    private final String archiveName;
    private final String source;
    private final Date start;
    private final Date end;
    private final String cronExpression;
    private final OfflineClient.ExecutionType executionType;

    @JsonCreator
    public TopologyJobInfo(@JsonProperty("name") String name,
            @JsonProperty("comment") String comment,
            @JsonProperty("topologyPath") String topologyPath,
            @JsonProperty("archiveName") String archiveName,
            @JsonProperty("source") String source,
            @JsonProperty("start") long start,
            @JsonProperty("end") long end,
            @JsonProperty("cronExpression") String cronExpression,
            @JsonProperty("executionType") OfflineClient.ExecutionType executionType) {

        this.name = name;
        this.comment = comment;
        this.topologyPath = topologyPath;
        this.archiveName = archiveName;
        this.source = source;
        this.start = new Date(start);
        this.end = new Date(end);
        this.cronExpression = cronExpression;
        this.executionType = executionType;
    }

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public String getTopologyPath() {
        return topologyPath;
    }

    public String getArchiveName() {
        return archiveName;
    }

    public String getSource() {
        return source;
    }

    @JsonIgnore
    public Date getStart() {
        return start;
    }

    @JsonProperty("start")
    public String getStartStr() {
        return start.toString();
    }

    @JsonIgnore
    public Date getEnd() {
        return end;
    }

    @JsonProperty("end")
    public String getEndStr() {
        return end.toString();
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public OfflineClient.ExecutionType getExecutionType() {
        return executionType;
    }
}
