package ui;

import offine.abstraction.OfflineClient;
import offine.implementation.DefaultOfflineClient;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

@Path("/offline")
public class OfflineResource {
    private final OfflineClient client;

    public OfflineResource() {
        client = new DefaultOfflineClient();
    }

    @Path("/list")
    @GET
    public Response list() throws IOException {
        return Response.ok(client.list()).build();
    }

    @Path("/upload")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response upload(@FormDataParam("name") String name,
            @FormDataParam("topology_path") String topology_path,
            @FormDataParam("topology_archive") InputStream stream,
            @FormDataParam("topology_archive") FormDataContentDisposition file,
            @FormDataParam("source") String source,
            @FormDataParam("start") String start,
            @FormDataParam("end") String end,
            @FormDataParam("cron_expression") String cron_expression,
            @FormDataParam("execution_type") OfflineClient.ExecutionType type) throws IOException {


        client.upload()
                .name(name, "some comments")
                .cron(cron_expression)
                .path(topology_path)
                .from(source, new Date(Long.valueOf(start)), new Date(Long.valueOf(end)))
                .execute();

        return Response.ok().build();
    }
}
